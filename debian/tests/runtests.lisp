(require "asdf")

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "cl-ppcre")
  (asdf:load-system "cl-ppcre/test")
  (asdf:load-system "cl-ppcre-unicode")
  (asdf:load-system "cl-ppcre-unicode/test"))

;; Can't use ASDF:TEST-SYSTEM, its return value is meaningless
(unless (cl-ppcre-test:run-all-tests :more-tests 'cl-ppcre-test:unicode-test)
  (uiop:quit 1))
